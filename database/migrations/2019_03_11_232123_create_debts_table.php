<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('debts')) {
            return;
        }

        Schema::create('debts', function (Blueprint $table) {
            $table->increments('id_debts');
            $table->integer('id_clients')->nullable(false);
            $table->decimal('amount', 10, 2)->nullable(false);
            $table->decimal('collection_fees', 10, 2)->nullable(false);
            $table->decimal('moratory_fees', 10, 2)->nullable(false);
            $table->date('next_try_to_charge')->nullable(false);
            $table->string('id_charges', 64)->default(NULL);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->index('id_clients', 'id_clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('debts')) {
            Schema::table('debts', function (Blueprint $table) {
                $table->dropIndex('id_clients');
            });
            Schema::dropIfExists('debts');
        }
    }
}
