<?php namespace App;

use App\IClientRepository;
use App\ClientService;
use App\Level;

class CommissionServiceOld {
	/**
	 * $clientRepository
	 * 
	 * @var IClientRepository
	 */
	private $clientRepository;

	/**
	 * $clientService
	 * 
	 * @var ClientService
	 */
	private $clientService;

	/**
	 * __construct
	 * Constructor e inicializador de variables necesarias.
	 *
	 * @require IClientRepository App\IClientRepository
	 * @require ClientService     App\ClientService
	 * @param IClientRepository $clientRepository 
	 * @param ClientService     $clientService    
	 * @return void
	 */
	public function __construct(IClientRepository $clientRepository, ClientService $clientService) {

		$this->clientRepository = $clientRepository;
		$this->clientService = $clientService;

	}

	/**
	 * calculateCommissions
	 * Función para calcular las comisiones de un cliente.
	 *   1) Revisa si el cliente se encuentra activo para comisiones.
	 *   2) Revisa el novel del cliente
	 *   3) Dependiendo del nivel (App\Level), le asigna una comisión de uno o más usuarios.
	 * @param  Client $client [description]
	 * @return array          Array con los datos:
	 *                          is_active: Se encuentra activo.
	 *                          itemization: Los Clientes registrados a este usuario.
	 *                          total: El monto total de comisiones.
	 */
	public function calculateCommissions(Client $client) {

		$isActive = $this->clientService->isActive($client->id_clients);

		switch($client->level) {

			case Level::VIAJERO:

				$itemization = $this->clientRepository->getSignedClientsTree($client->id_clients);
				$response['is_active'] = $isActive;
				$response['itemization'] = $itemization;
				$response['total'] = 0;

			break;

			case Level::MARINERO:
				
				$itemization = $this->clientRepository->getSignedClientsTree($client->id_clients);
				$response['is_active'] = $isActive;
				$response['itemization'] = $itemization;
				$response['total'] = $isActive == false ? 0 : $itemization['level_1'] * 30;

			break;
			
			case Level::CABO:

				$itemization = $this->clientRepository->getSignedClientsTree($client->id_clients);
				$response['is_active'] = $isActive;
				$response['itemization'] = $itemization;
				$response['total'] = $isActive == false ? 0 :  $itemization['level_1'] * 30 +
									 						   $itemization['level_2'] * 15;

				break;

			case Level::CAPITAN:

				$itemization = $this->clientRepository->getSignedClientsTree($client->id_clients);
				$response['is_active'] = $isActive;
				$response['itemization'] = $itemization;
				$response['total'] = $isActive == false ? 0 :  $itemization['level_1'] * 30 +
									 						   $itemization['level_2'] * 15 +
									 						   $itemization['level_3'] * 10;

			break;

			case Level::ALMIRANTE:

				$itemization = $this->clientRepository->getSignedClientsTree($client->id_clients);
				$response['is_active'] = $isActive;
				$response['itemization'] = $itemization;
				$response['total'] = $isActive == false ? 0 :  $itemization['level_1'] * 30 +
									 						   $itemization['level_2'] * 15 +
									                           $itemization['level_3'] * 10 +
									                           $itemization['level_4'] * 5;
			break;

		}

		return $response;

	}

}