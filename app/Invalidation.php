<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invalidation extends Model {

	protected $table = 'invalidations';
	protected $primaryKey = 'id_invalidations';
	
}