<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model {

    protected $table = 'confirmations';
    protected $primaryKey = 'id_confirmations';

}
