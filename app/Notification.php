<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

	protected $table = 'notifications';
	protected $primaryKey = 'id_notifications';
	protected $fillable = ['id_users','type', 'message'];

	const PUSH = 1;
	const EMAIL = 2;

}
