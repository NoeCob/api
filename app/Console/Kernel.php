<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\DailyCharge::class,
        \App\Console\Commands\CalculateCommissions::class,
        \App\Console\Commands\GenerateInvoices::class,
        \App\Console\Commands\CleanResidualDebt::class,
        \App\Console\Commands\CollectDebt::class,
        \App\Console\Commands\CreateFilterChangeTicket::class,
        \App\Console\Commands\GetInvoicesEmail::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('inspire')->everyMinute()->appendOutputTo(storage_path().'/logs/cron.log');
/*
        $schedule->command('charges:run')->dailyAt('00:05')->appendOutputTo(storage_path().'/logs/cron.log');
        $schedule->command('clients:clear-residual-debt')->dailyAt('23:30');

        $schedule->command('clients:filter-change')
                 ->dailyAt('00:10')
                 ->when(function () {
                      return \Carbon\Carbon::now()->startOfMonth()->isToday();
                 })
                 ->appendOutputTo(storage_path().'/logs/filterchange.log');

        $schedule->command('invoices:generate')
                 ->dailyAt('23:40')
                 ->when(function () {
                      return \Carbon\Carbon::now()->endOfMonth()->isToday();
                 })
                 ->appendOutputTo(storage_path().'/logs/invoices.log');
                 
        $schedule->command('commission:calculate')
                 ->dailyAt('23:50')
                 ->when(function () {
                      return \Carbon\Carbon::now()->endOfMonth()->isToday();
                 })
                 ->appendOutputTo(storage_path().'/logs/commission.log');
        */
    }
}
