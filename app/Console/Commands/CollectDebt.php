<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ClientService;
use App\Client;
use App\Charge;
use App\Commission;
use App\CommissionService;
use App\Debt;
use App\MySQLClientRepository;
use App\Traits\DebtTrait;
use App\Traits\NotifyTrait;
use Carbon\Carbon;
use Conekta;
use Conekta_Customer;
use Conekta_Charge;
use Conekta_Event;
use Conekta_Plan;
use Conekta_ResourceNotFoundError;
use Conekta_ProcessingError;
use Conekta_Handler;

class CollectDebt extends Command
{
    use DebtTrait, NotifyTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'charges:collectDebt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect the Debt for clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * 1) Obtiene todos los clientes que tengan que pagar.
     * 2) Obtiene las deudas y cargos de cada cliente.
     * 3) Verifica fecha del ultimo pago, para ver tiene que realizar cargo o no.
     * 4) Antes de cobrar el mes, intenta cobrar las deudas que tenga antes.
     * 5) Verifica si tiene o no aguagratis.
     * 5.1) Si no, intenta realizar el cargo
     *
     * @return void
     */
    public function handle()
    {   
		$today         = Carbon::now()->format('Y-m-d');
        $exceptionList = [];
        $onlyRun       = [398,404,405,451,473,475,476,515,546,547];
		

        $this->info('Fecha: '.$today);
        $clients = Client::where('status', '=', 'accepted')
                            //->whereNotIn('id_clients', $exceptionList)
                            ->whereIn('id_clients', $onlyRun)
                            ->get();
        
        foreach ($clients as $client) {
            if ($client->status == 'accepted') {
                $debts = Debt::where('id_clients', '=', $client->id_clients)
                             ->whereNull('id_charges')
                             //->where('next_try_to_charge', '<=', $today)
                             ->get();

                //intenta cobrar las deudas que tenga el cliente primero.
                $debtsCharged = [];
                foreach ($debts as $debt) {
                    $debtsCharged[] = $this->chargeDebt($debt->id_debts);
                }

                $this->info('Cliente: ('.$client->id_clients.') '.$client->name.print_r($debtsCharged, true));
            }
        }
    }
}
