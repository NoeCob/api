<?php

namespace App\Http\Controllers;

use App\Post;
use App\Http\Controllers\Main;
use DB;
use Request;
use Validator;

class PostsController extends Main
{
    public function __construct()
    {
        parent::__construct(['index']);
    }
    /**
     * index
     * Devuelve todas los posts (\App\Post) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Post
     * 
     * @return response Ok|Internal Server Error
     */
    public function index() {

        try {

            $posts = Post::query();

            foreach(Request::query() as $name => $value) {

                $posts = $posts->where($name, $value);

            }

            return Main::response(true, 'OK', $posts->orderBy('id_posts', 'DESC')->get(), 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    /**
     * create
     * Crea un nuevo post.
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Post
     * 
     * @return response     Created|Bad Request|Internal Server Error
     */
    public function create() {

        try {

            $input = Request::all();

            $validator = Validator::make(
                $input,
                [
                    'title'  => 'string',
                    'text'   => 'string',
                    'image'  => 'image|min:1'
                ]
            );

            if($validator->fails()) {

                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

            }

            $post = new Post;
            $post->title = @ $input['title'];
            $post->text = @ $input['text'];
            $post->save();

            if(isset($input['image'])) {

                $imageName = $post->id_posts . '.' . Request::file('image')->getClientOriginalExtension();

                Request::file('image')->move(
                    "images/posts/$post->id_posts/",
                    $imageName
                );

                $post->image = env('URL_API') . "/images/posts/$post->id_posts/$imageName";
                $post->save();

            }

            return Main::response(true, 'Created', $post, 201);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    /**
     * show
     * Muestra un post (\App\Post) por medio del ID
     *
     * @\App\Post
     * 
     * @param  int      $id ID del post
     * @return response     OK|Not Found(404)
     */
    public function show($id) {

        $post = Post::find($id);

        if($post) {

            return Main::response(true, 'OK', $post);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * update
     * Guarda las modificaciones realizadas a un post (\App\Post) por medio del ID
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Post
     * 
     * @param  int      $id ID del post
     * @return response     OK|Bad Request|Not Found
     */
    public function update($id) {

        $post = Post::find($id);

        if($post) {

            try {

                $input = Request::all();

                $validator = Validator::make(
                    $input,
                    [
                        'title'          => 'string',
                        'text'           => 'string',
                        'image'          => 'image|min:1'
                    ]
                );

                if($validator->fails()) {

                    return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

                }

                $post->title = @ $input['title'];
                $post->text = @ $input['text'];
                if(isset($input['image'])) {

                    $images = glob("images/posts/$post->id_posts/*");
                    foreach($images as $image)
                        unlink($image);

                    $imageName = $post->id_posts . '.' . Request::file('image')->getClientOriginalExtension();

                    Request::file('image')->move(
                        "images/posts/$post->id_posts/",
                        $imageName
                    );

                    $post->image = env('URL_API') . "/images/posts/$post->id_posts/$imageName";
                    $post->save();

                } else {

                    $images = glob("images/posts/$post->id_posts/*");
                    foreach($images as $image)
                        unlink($image);
                    $post->image = null;

                }

                $post->save();

                return Main::response(true, 'OK', $post, 200);

            } catch(\Exception $e) {

                return Main::response(false, 'Not Found', null, 400);

            }

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * destroy
     * Borra de la base de datos un post(\App\Post) por medio de un ID, y elimina las imágenes relacionadas
     * 
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Post
     * 
     * @param  int      $id ID del post
     * @return response     Ok|Not Found
     */
    public function destroy($id) {

        $post = Post::find($id);

        if($post) {

            $images = glob("images/posts/$post->id_posts/*");
            foreach($images as $image)
                unlink($image);
            $post->delete();

            return Main::response(true, 'OK', null, 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

}
