<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Main;
use DB;
use Request;
use App\ClientService;
use App\Client;
use App\Commission;
use App\CommissionService;
use App\MySQLClientRepository;

class CommissionsController extends Main {

    /**
     * index
     * Las comisiones son todos los porcentajes de cada pago de cliente referido, dependiendo de su nivel.
     * Devuelve todas las comisiones (\App\Comission) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Comission
     * 
     * @return response OK|Internal Server Error
     */
    public function index() {

        try {

            $commissions = Commission::query();

            foreach(Request::query() as $name => $value) {
                
                switch($name) {

                    case 'from':
                        
                        $commissions = $commissions->where('calculated_at', '>=', $value);

                    break;

                    case 'to':
                        
                        $commissions = $commissions->where('calculated_at', '<=', $value);

                    break;

                    default:

                        $commissions = $commissions->where($name, $value);

                    break;

                }

            }
            //dd($commissions->toSql());
            $commissions = $commissions->get();

            foreach($commissions as &$commission) {

                $this->resolveRelations($commission);
                
            }

            return Main::response(true, 'OK', $commissions);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }
        
    }

    /**
     * registerAsPaid
     * Se marca una comisión (\App\Comission) como pagada al asignarle una fecha en "paid_at", por medio
     *  de un ID.
     * 
     * @App\Comission
     * 
     * @param  int      $id ID de comisión
     * @return response     OK|Not Found
     */
    public function registerAsPaid($id) {

        $commission = Commission::find($id);

        if($commission) {

            $commission->paid_at = date('Y-m-d H:i:s');
            $commission->save();

            return Main::response(true, 'OK', $commission, 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * resolveRelations
     * Asigna el cliente (\App\Client) a la comisión, por medio del ID de cliente(id_clients)
     *
     * @App\Comission
     * @App\Client
     * 
     * @param  Commission $commission Objeto Eloquent Category
     * @return Commission           Objeto Eloquent Category
     */
    private function resolveRelations(Commission $commission) {

        $commission->client = Client::find($commission->id_clients);
        return $commission;

    }

    /**
     * calculateCommissions
     * Esta función llama al servicio \App\ComissionService para calcular las comisiones por cada cliente.
     *  Devuelve el listado de clientes con las comisiones asignadas, así como los links para la paginación.
     *  
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Comission
     * @App\ComissionService
     * @App\Client
     * 
     * @return response OK
     */
    public function calculateCommissions() {

        $query = Request::query();

        $result = Client::paginate(
                    isset($query['limit']) ?
                          $query['limit'] : 20
                    );

        $clients = $result->items();
        $clientRepository = new MySQLClientRepository();
        $commissionService = new CommissionService(
            $clientRepository,
            new ClientService($clientRepository)
        );

        foreach ($clients as &$client) {
            
            $client->commissions = $commissionService->calculateCommissions($client);

        }

                                        $links[] = "<{$result->url($result->lastPage())}>; rel=\"last\"";
                                        $links[] = "<{$result->url(1)}>; rel=\"first\"";
        if($result->nextPageUrl())      $links[] = "<{$result->nextPageUrl()}>; rel=\"next\"";
        if($result->previousPageUrl())  $links[] = "<{$result->previousPageUrl()}>; rel=\"prev\"";

        return Main::response(true, 'OK', $clients, 200, [ "Link" =>  implode(',', $links) ]);

    }

    /**
     * getClientCommissionUpToToday
     * Calcula las comisiones de un agente de ventas al día de hoy.
     *
     * @param Request $request
     * @param int $id_clients   ID del cliente
     * @return Response
     */
    public function getClientCommissionUpToToday(Request $request, $id_clients)
    {
        //$id       = $id_clients;
        $main     = Client::where('id_clients', $id_clients)->first();
        $cs       = new CommissionService();
        $response = [
            'success'  => false,
            'message'  => 'Not Found',
            'response' => [],
            'status'   => 404
        ];

        if ($main) {
            $response = [
                'success'  => true,
                'message'  => '',
                'response' => $cs->calculateCommissions($main),
                'status'   => 200
            ];
        }

        return Main::response($response['success'], $response['message'], $response['response'], $response['status']);
    }

    /**
     * getClientLoosingCommission
     * Calcula las comisiones de un agente de ventas al día de hoy, como si fuera el nivel más alto.
     *
     * @param Request $request
     * @param int $id_clients   ID del cliente
     * @return Response
     */
    public function getClientLoosingCommission(Request $request, $id_clients)
    {
        $main     = Client::where('id_clients', $id_clients)->first();
        $cs       = new CommissionService();
        $response = [
            'success'  => false,
            'message'  => 'Not Found',
            'response' => [],
            'status'   => 404
        ];

        if ($main) {
            $response = [
                'success'  => true,
                'message'  => '',
                'response' => $cs->calculateCommissions($main, null, true),
                'status'   => 200
            ];
        }

        return Main::response($response['success'], $response['message'], $response['response'], $response['status']);
    }

    public function getDeclinedClientsPerCommission(Request $request, $id_commissions)
    {
        $commission = Commission::where('id_commissions', '=', $id_commissions)->first();
        $lvl =  [];
        foreach ($commission->declined as $level => $info) {
            foreach ($info as $id_clients => &$values) {
                $client         = Client::where('id_clients', '=', $id_clients)->first();
                $values['name'] = $client->name;
                $lvl[$level]    = $info;
            }
        }
        return response()->json($lvl);
    }

}
