<?php

namespace App\Http\Controllers;

use App\Element;
use App\Http\Controllers\Main;
use DB;
use Request;
use Validator;

class ElementsController extends Main {

    /**
     * __construct
     * Se le indica que la funcion "index" no debe usar AUTH, por medio del controlador MAIN
     */
    public function __construct() {

        parent::__construct(['index']);

    }

    /**
     * index
     * Los categoriesElements son las opciones que puede tener cada categoría, como puede ser el tipo de barrenado
     *  o las instalaciones extras.
     * Devuelve todas los categoriesElements activos después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response NULL|Error(404)
     */
    public function index() {

        try {

            $elements = DB::table('categories_elements');

            foreach(Request::query() as $name => $value) {

                $elements = $elements->where($name, $value);

            }

            $elements = $elements->where('active', 1)->get();

        } catch(\Exception $e) {

            return Main::response(false, null, null, 400);

        }

        return Main::response(true, null, $elements);

    }

    /**
     * create
     * Crea un categoriesElements y le asigna la relación a la categoría.
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Element
     * 
     * @return response     NULL|Bad Request|Error
     */
    public function create() {

        try {

            $input = Request::all();

            $validator = Validator::make(
                $input,
                [
                    'id_categories'  => 'required|integer',
                    'name'           => 'required|string',
                    'price'          => 'required|numeric',
                    'image'          => 'image|min:1'
                ]
            );

            if($validator->fails()) {

                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

            }

            $element = new Element;
            $element->id_categories = $input['id_categories'];
            $element->name = $input['name'];
            $element->price = $input['price'];
            if(isset($input['image'])) {
                $element->type = $input['image']->getMimeType();
                $element->image = base64_encode(file_get_contents($input['image']->getRealPath()));
            }
            $element->save();

            return Main::response(true, null, $element, 201);

        } catch(\Exception $e) {

            return Main::response(false, $e->getMessage(), null, 400);

        }

    }

    /**
     * show
     * Muestra un categoriesElement (\App\Element) por medio del ID
     *
     * @\App\Element
     * 
     * @param  int      $id ID del categoriesElement
     * @return response     NULL|ERROR(404)
     */
    public function show($id) {

        $element = Element::find($id);

        if($element && $element->active == 1) {

            return Main::response(true, null, $element);

        } else {

            return Main::response(false, null, null, 404);

        }

    }

    /**
     * update
     * Guarda las modificaciones realizadas a un categoriesElement (\App\Element) por medio del ID
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Element
     * 
     * @param  int      $id ID del categoriesElement
     * @return response     NULL|EBad Request|RROR(404)
     */
    public function update($id) {

        $element = Element::find($id);

        if($element && $element->active == 1) {

            try {

                $input = Request::all();

                $validator = Validator::make(
                    $input,
                    [
                        'name'           => 'required|string',
                        'price'          => 'required|numeric',
                        'image'          => 'image|min:1'
                    ]
                );

                if($validator->fails()) {

                    return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

                }

                $element->name = $input['name'];
                $element->price = $input['price'];
                $element->type = $input['image']->getMimeType();
                $element->image = base64_encode(file_get_contents($input['image']->getRealPath()));
                $element->save();

                return Main::response(true, null, $element);

            } catch(\Exception $e) {

                return Main::response(false, null, null, 400);

            }

        } else {

            return Main::response(false, null, null, 404);

        }

    }

    /**
     * destroy
     * Desactiva una categoriesElement(\App\Element) por medio de un ID
     * 
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Element
     * 
     * @param  int      $id ID del categoriesElement
     * @return response     NULL|ERROR(404)
     */
    public function destroy($id) {

        $element = Element::find($id);

        if($element && $element->active == 1) {

            $element->active = 0;
            $element->save();

            return Main::response(true, null, null);

        } else {

            return Main::response(false, null, null, 404);

        }

    }

}
