<?php

namespace App\Http\Controllers;

use App\SubscriptionEvent;
use Request;

class SubscriptionEventsController extends Main {

    /**
     * index
     * Una suscripcion es....
     * Devuelve todas las suscripciones (\App\SubscriptionEvent) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\SubscriptionEvent
     * 
     * @return response Ok|Internal Server Error
     */
    public function index() {

        try {

            $events = SubscriptionEvent::query();

            foreach(Request::query() as $name => $value) {

                $events = $events->where($name, $value);

            }

            return Main::response(true, 'OK', $events->orderBy('date', 'desc')->get(), 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

}
