<?php

namespace App\Http\Controllers;

use App\Assignation;
use App\Http\Controllers\Main;
use App\Ticket;
use App\Client;
use App\ErrorCode;
use App\User;
use Request;
use Validator;

class AssignationsController extends Main {

    /**
     * __construct
     * Se le indica que la funcion "show" no debe usar AUTH, por medio del controlador MAIN
     */
    public function __construct() {

        parent::__construct(['show']);

    }
    
    /**
     * index
     * Las asignaciones son los elementos que pertenecen a un ticket.
     * Devuelve todas las asignaciones (\App\Assignation) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response OK|Internal Server Error
     */
    public function index() {
        
        try {

            $assignations = Assignation::query();

            foreach(Request::query() as $name => $value) {
                
                $assignations = $assignations->where($name, $value);

            }

            $assignations = $assignations->get();

            foreach($assignations as &$assignation) {

                $assignation = $this->resolveRelations($assignation);
            
            }


            return Main::response(true, 'OK', $assignations, 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }
        
    }

    /**
     * show
     * Devuelve una asignación(\App\Assignation) por medio del ID.
     * 
     * @param  int $id 
     * @return reponse     OK|Not Found
     */
    public function show($id) {

        if($assignation = Assignation::find($id)) {

            return Main::response(true, 'OK', $this->resolveRelations($assignation), 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * resolveRelations
     * Busca el ticket(\App\Ticket) relacionado a la Asignación(\App\Assignation) y llena la información
     *  necesaria como:
     *  - Ticket (\App\Ticket)
     *  - Cliente (\App\Client)
     *  - Error (\App\ErrorCode)
     *  - Usuario que lo cerró (\App\User)
     *  - Técnico (\App\User)
     * 
     * @\App\Ticket
     * @\App\Client
     * @\App\ErrorCode
     * @\App\User
     * 
     * @param  Assignation $assignation [description]
     * @return Assignation               
     */
    private function resolveRelations(Assignation $assignation) {

        $ticket = Ticket::find($assignation->id_tickets);
        $ticket->client = Client::find($ticket->id_clients);
        $ticket->error_code = ErrorCode::find($ticket->id_error_codes);
        $ticket->closed_by = User::find($ticket->id_closed_by);

        $assignation->ticket = $ticket;
        $assignation->technician = User::find($assignation->id_technicians);

        return $assignation;

    }

}
