<?php namespace App;

use App\Client;
use App\IClientRepository;
use App\Level;
use App\StandardType;
use DB;
use Conekta;
use Conekta_Customer;

class LevelService {
	
	/**
	 * $clientRepository
	 * @var IClientRepository
	 */
	private $clientRepository;

	/**
	 * __construct
	 * Constructor del modelo, se inicializa la variable: $clientRepository
	 * @require IClientRepository App\IClientRepository
	 * @param IClientRepository $clientRepository
	 * @return void
	 */
	public function __construct(IClientRepository $clientRepository) {

		$this->clientRepository = $clientRepository;

	}

	/**
	 * setLevels
	 * Asigna nivel (App\Level) al usuario, dependiendo de cuantos referidos tiene.
	 * Por cada cliente referido:
	 *   1) Obtiene los clientes que sean referidos(refered_by) por el cliente.
	 *   2) Obtiene la cantidad de clientes que ya estén refiriendo a otros clientes, es decir, que tengan
	 *   	más de 6 clientes referidos.
	 *   3) Revisa en CONEKTA, cuantos clientes relamente están activos.
	 *   4) Dependiento de la cantidad de clientes activos y referidos, se le cambia el nivel al cliente
	 * @param Client $client [description]
	 * @return void
	 */
	public function setLevels(Client $client) {

		do {

			$payingClients = $this->clientRepository->getPayingClients($client->id_clients);
			
	        $nPayingClients = count($payingClients);

	        $nFreeWater = 0;

	        foreach($payingClients as $payingClient) {

	        	if ($payingClient->level !== Level::VIAJERO) {

	        		$nFreeWater++;

	        	}

	        }

	        Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
            Conekta::setLocale('es');

            $customer = Conekta_Customer::find($client->conekta_token);

	        if($nPayingClients >= 6) {

	        	if(isset($customer->subscription) &&
	        			 $customer->subscription->status == 'active') {

					$customer->subscription->pause();

	        	}

	        } else {

				if(isset($customer->subscription) &&
					     $customer->subscription->status == 'paused') {

					$customer->subscription->resume();

				}

	        }

	        if($nPayingClients >= 16 && $nFreeWater >= 15) {

	        	$client->level = Level::ALMIRANTE;

	        } else if($nPayingClients >= 16 && $nFreeWater >= 10) {

	        	$client->level = Level::CAPITAN;

	        } else if($nPayingClients >= 16) {

	        	$client->level = Level::CABO;

	        } else if($nPayingClients >= 6) {

	        	$client->level = Level::MARINERO;

	        } else {

	        	$client->level = Level::VIAJERO;

	        }

	        $this->clientRepository->save($client);

		} while($client = $this->clientRepository->getById($client->referred_by));

	}

}



