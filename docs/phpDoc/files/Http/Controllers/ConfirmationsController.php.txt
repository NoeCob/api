<?php

namespace App\Http\Controllers;

use App\Confirmation;
use App\Http\Controllers\Main;
use App\Ticket;
use Request;
use Validator;

class ConfirmationsController extends Main {

    /**
     * index
     * Las confirmaciones son la interaccion del cliente con su ticket, para poder finalizarlo.
     * Devuelve todas las confirmaciones (\App\Confirmation) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Confirmation
     * 
     * @return response OK|Internal Server Error
     */
    public function index() {

        try {

            $confirmations = Confirmation::query();

            foreach(Request::query() as $name => $value) {
                
                $confirmations = $confirmations->where($name, $value);

            }

            $confirmations = $confirmations->get();

            foreach($confirmations as &$confirmation) {

                $confirmation = $this->resolveRelations($confirmation);
            
            }

            return Main::response(true, 'OK', $confirmations, 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }
        
    }

    /**
     * create
     * Crea una confirmación y la relaciona con un ticket por medio de id_tickets.
     * Por ultimo, el ticket se marca como "confirmed".
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @Illuminate\.....\Validator
     * @\App\Confirmation
     * @\App\Ticket
     * 
     * @return void
     */
    public function create() {

        $input = Request::all();

        $validator = Validator::make(
            $input,
            [
                'id_tickets'        => 'required|exists:tickets',
                'client_comments'   => 'string'
            ]
        );

        if($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

        $ticket = Ticket::find($input['id_tickets']);

        $confirmation = new Confirmation();
        $confirmation->id_tickets = $ticket->id_tickets;
        $confirmation->client_comments = @ $input['client_comments'];
        $confirmation->save();

        $ticket->status = 'confirmed';
        $ticket->save();

    }

    /**
     * show
     * Muestra una confirmación (\App\Confirmation) por medio del ID
     *
     * @\App\Confirmation
     * 
     * @param  int      $id ID de confirmación
     * @return response     OK|Not Found
     */
    public function show($id) {

        if($confirmation = Confirmation::find($id)) {

            return Main::response(true, 'OK', $this->resolveRelations($confirmation), 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * resolveRelations
     * Busca y asigna el ticket relacionado a la confirmación por medio de id_tickets
     * 
     * @param  Confirmation $confirmation Objeto Eloquent Confirmation
     * @return Confirmation           Objeto Eloquent Category
     */
    private function resolveRelations(Confirmation $confirmation) {

        $confirmation->ticket = Ticket::find($confirmation->id_tickets);

        return $confirmation;

    }

}

