<?php

namespace App\Http\Controllers;

use App\Cancellation;
use App\Http\Controllers\Main;
use DB;
use Request;

class CancellationsController extends Main {

    /**
     * index
     * Devuelve todas las cancelaciones (\App\Cancellation) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response NULL|Internal Server Error
     */
    public function index() {
        
        try {

            $cancellations = DB::table('cancellations');

            foreach(Request::query() as $name => $value) {
                
                $cancellations = $cancellations->where($name, $value);

            }

            return Main::response(true, null, $cancellations->get());

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', null, 500);

        }
   
    }

    /**
     * show
     * Devuelve una cancelación(\App\Cancellation) por medio del ID.
     * 
     * @param  int $id
     * @return response     OK|NOT FOUND
     */
    public function show($id) {

        if($cancellation = Cancellation::find($id)) {

            return Main::response(true, 'OK', $cancellation);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

}
