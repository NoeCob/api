<?php

return array(

    'aguagente-push-ios'     => array(
        'environment' => env('PUSH_ENV', 'development'),
        'certificate' =>'../aguagente-push-ios.pem',
        'service'     =>'apns'
    ),
    'aguagente-push-android' => array(
        'environment' => env('PUSH_ENV', 'development'),
        'apiKey'      =>'AIzaSyBIHYS5hD6aRz395D77m8jjbAz3zJg5EUw',
        'service'     =>'gcm'
    )

);
