<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class UserTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * User Index test.
     *
     * @return void
     */
    public function testUsersIndex()
    {
        $this->get('/users')
            ->assertResponseStatus(200);
    }

    /**
     * User register test.
     *
     * @return void
     */
    public function testUserRegister()
    {
        $name = 'test_name' . strtotime("now");
        $email = 'test_' . strtotime("now") . '@test.com';
        $password = 'test_1';
        $params = [
            "name" => $name,
            "email" => $email,
            "password" => $password,
            "status" => 1
        ];
        $this->post('/auth/register', $params)
            ->assertResponseStatus(200);
    }

    public function testUserUpdate()
    {
        $user_last = User::latest()->first();
        if (!empty($user_last)) {
            $name = 'test_name' . strtotime("now");
            $password = 'test_1';
            $params = [
                "name" => $name,
                "password" => $password,
                "phone" => '9993445453'
            ];
            $this->put('/users/' . $user_last->id, $params)
                ->assertResponseStatus(200);
        }
    }

    /**
     * User Show test.
     *
     * @return void
     */
    public function testUserShow()
    {
        $user_last = User::latest()->first();
        if (!empty($user_last)) {
            $this->get('/users/' . $user_last->id)
                ->assertResponseStatus(200);
        }

    }

    public function testUserToogleStatus()
    {
        $technicians = $this->get('/users?role=Technician');
        $data = json_decode($technicians->response->content());
        $index = sizeof($data->response) - 1;
        $this->get('/users/toogleStatus/' . $data->response[$index]->id)
            ->assertResponseStatus(200);
    }

    public function testRegisterDevice()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->withSession(['foo' => 'bar']);

        $devices = ['ios', 'android'];
        $index = rand(0, 1);
        $params = [
            'registration_id' => 'test_registration_id' . strtotime("now"),
            'platform' => $devices[$index]
        ];
        $this->post('/users/registerDevice', $params)
            ->assertResponseStatus(200);
    }
}
