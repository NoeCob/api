<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InvalidationsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testInvalidationIndex()
    {
        $this->get('/invalidations')
            ->assertResponseStatus(200);
    }

    public function testInvalidationShow()
    {
        $invalidation = $this->getObjectRandom(\App\Invalidation::class);
        $this->get('/invalidations/' . $invalidation->id_invalidations)
            ->assertResponseStatus(200);
    }

}
