<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ElementsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Contract index test
     *
     * @return void
     */
    public function testElementIndex()
    {
        $this->get('/elements')
            ->assertResponseStatus(200);
    }

    public function testElementCreate()
    {
        $params = $this->getTestData('/data/elements/create.json');
        $category = $this->getObjectRandom(\App\Category::class);
        $params['id_categories'] = $category->id_categories;
        $this->post('/elements', $params)
            ->assertResponseStatus(201);
    }

    public function testElementUpdate()
    {
        // image test
        $url = 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
        $name = str_random(8).'.png';
        $path = sys_get_temp_dir().'/'.$name;
        copy($url, $path);
        $params = $this->getTestData('/data/elements/update.json');
        $params['image'] = new \Symfony\Component\HttpFoundation\File\UploadedFile($path, $name,
            'image/png', filesize($path), null, true);
        $element = $this->getObjectRandom(\App\Element::class);
        while ($element->active != 1) {
            $element = $this->getObjectRandom(\App\Element::class);
        }
        $response = $this->put('/elements/' . $element->id_categories_elements, $params)
            ->assertResponseStatus(200);

    }

    public function testElementDestroy()
    {

        $element = $this->getObjectRandom(\App\Element::class);
        while ($element->active != 1) {
            $element = $this->getObjectRandom(\App\Element::class);
        }
        $this->delete('/elements/' . $element->id_categories_elements)
            ->assertResponseStatus(200);
    }

}
