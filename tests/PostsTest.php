<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Post index test
     *
     * @return void
     */
    public function testPostsIndex()
    {
        $this->get('/posts')
            ->assertResponseStatus(200);
    }

    public function testPostsCreate()
    {
        $params = $this->getTestData('/data/posts/data.json');
        $this->post('/posts', $params)
            ->assertResponseStatus(201);
    }

    public function testPostsUpdate()
    {
        $params = $this->getTestData('/data/posts/data.json');
        $post = $this->getObjectRandom(\App\Post::class);
        $this->put('/posts/' . $post->id_posts, $params)
            ->assertResponseStatus(200);
    }

    public function testPostsShow()
    {
        $params = $this->getTestData('/data/posts/data.json');
        $post = $this->getObjectRandom(\App\Post::class);
        $this->get('/posts/' . $post->id_posts, $params)
            ->assertResponseStatus(200);
    }

    public function testPostsDestroy()
    {
        $post = $this->getObjectRandom(\App\Post::class);
        $this->delete('/posts/' . $post->id_posts)
            ->assertResponseStatus(200);
    }

}
