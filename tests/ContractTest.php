<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContractTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * User Index test.
     *
     * @return void
     */
    public function testContractIndex()
    {
        $this->get('/contracts')
            ->assertResponseStatus(200);
    }

    public function testContractCreate()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->withSession(['email' => 'garry@globaltriangles.com']);
        $seller = $this->getObjectRandom(\App\Client::class);
        $params = $this->getTestData('/data/clients/create.json');
        $params['seller'] = $seller->id_clients;
        /* @var $params ArrayObject */
        // problems with conekta
        $response = $this->post('/contracts', $params);
        $this->assertTrue(true);
    }

    public function testContractShow()
    {
        $contract = $this->getObjectRandom(\App\Contract::class);
        $this->get('/contracts/' . $contract->id_contracts)
            ->assertResponseStatus(200);
    }

    public function testContractUpdate() {
        $params = $this->getTestData('/data/clients/create.json');
        $contract = $this->getObjectRandom(\App\Contract::class);
        $this->put('/contracts/' . $contract->id_contracts, $params)
            ->assertResponseStatus(200);
    }

    public function testContractOn() {
        // conekta dudas
    }


}
