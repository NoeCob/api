<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * User Index test.
     *
     * @return void
     */
    public function testLogin()
    {

        $params = $this->getTestData('/data/users/login.json');
        /* @var $params ArrayObject */
        $this->post('/auth/login', $params)
            ->assertResponseStatus(200);
    }

    /**
     *
     * Logout test
     *
     * @return void
     */
    public function testLogout()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->withSession(['foo' => 'bar']);
        $this->get('auth/logout')
            ->assertResponseStatus(200);
    }

    public function testGetToken()
    {
        $this->get('/auth/token')
            ->assertResponseStatus(200);

    }

    public function testEmail()
    {
        $params = [
            'email' => 'garry@globaltriangles.com'
        ];
        $this->post('/auth/email', $params)
            ->assertResponseStatus(200);
    }

    public function testReset()
    {
        $params = [
            'nombre'    => 'testing',
            'correo'    => 'testin@gmail.com',
            'telefono'  => '9999232323',
            'interesa'  => 'test',
            'mensaje'   => 'messsage test'
        ];

        $this->post('/auth/unregistered', $params)
            ->assertResponseStatus(200);
    }


}
