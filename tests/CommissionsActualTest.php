<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommissionsActualTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testCommissionIndex()
    {
        $this->get('/commissions')
            ->assertResponseStatus(200);
    }

    public function testAssignationShow()
    {
        $commission = $this->getObjectRandom(\App\Commission::class);
        $this->post('/commissions/' . $commission->id_commissions . '/registerAsPaid')
            ->assertResponseStatus(200);
    }

    public function testCalculateCommissions()
    {
        $this->get('/commissions/calculateCommissions')
            ->assertResponseStatus(200);
    }

    public function testGetCommissions()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        $this->get('/commissions/getCommissions/' . $client->id_clients)
            ->assertResponseStatus(200);
    }

    public function testGetLoosingCommissions()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        $this->get('/commissions/getLoosingCommissions/' . $client->id_clients)
            ->assertResponseStatus(200);
    }

    public function testGetDeclined()
    {
        $commission = $this->getObjectRandom(\App\Commission::class);
        $this->post('/commissions/getDeclined/' . $commission->id_commissions)
            ->assertResponseStatus(200);
    }

}
