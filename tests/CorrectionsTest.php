<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CorrectionsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testCorrectionIndex()
    {
        $this->get('/corrections')
            ->assertResponseStatus(200);
    }

    public function testCorrectionShow()
    {
        $cancellation = $this->getObjectRandom(\App\Cancellation::class);
        $this->get('/corrections/' . $cancellation->id_cancellations)
            ->assertResponseStatus(200);
    }

}
