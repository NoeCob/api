<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Client index test
     *
     * @return void
     */
    public function testClientsIndex()
    {
        $this->get('/clients')
            ->assertResponseStatus(200);
    }

    public function testClientsUpdate()
    {
        $params = $this->getTestData('/data/clients/data.json');
        $client = $this->getObjectRandom(\App\Client::class);
        $this->put('/clients/' . $client->id_clients, $params)
            ->assertResponseStatus(200);
    }

    public function testClientsShow()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        $this->get('/clients/' . $client->id_clients)
            ->assertResponseStatus(200);
    }

    public function testClientsDestroy()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        while ($client->status != 'rejected') {
            $client = $this->getObjectRandom(\App\Client::class);
        }
        $this->delete('/clients/' . $client->id_clients)
            ->assertResponseStatus(200);
    }

    public function testClientsStatus()
    {
        $status = [
            'invalid', 'rejected', 'accepted', 'canceled', 'standby'
        ];
        $statusIndex = rand(0, 4);
        $params = [
            'status' => $status[$statusIndex],
            'reasons' => 'reason test'
        ];
        $client = $this->getObjectRandom(\App\Client::class);
        $this->post('clients/' . $client->id_clients . '/setStatus', $params)
            ->assertResponseStatus(200);

    }

    public function testClientSetGroup()
    {

        $group = $this->getObjectRandom(\App\Group::class);
        $params = [
            'id_groups' => $group->id_groups
        ];
        $client = $this->getObjectRandom(\App\Client::class);
        $this->post('clients/' . $client->id_clients . '/setGroup', $params)
            ->assertResponseStatus(200);

    }

    public function testClientSubscriptionPlan()
    {
        // conekta error key
        $client = $this->getObjectRandom(\App\Client::class);
        $this->post('clients/' . $client->id_clients . '/subscribeToPlan')
            ->assertResponseStatus(200);
    }

    public function testClientSubscribeToPlanOffline()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        $this->post('clients/' . $client->id_clients . '/subscribeToPlanOffline')
            ->assertResponseStatus(200);
    }

    public function testUnsubscribeToPlan()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        $this->post('clients/' . $client->id_clients . '/unsubscribeToPlan')
            ->assertResponseStatus(200);
    }

    public function testHierarchicalTree()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        $this->get('clients/' . $client->id_clients . '/hierarchicalTree')
            ->assertResponseStatus(200);
    }

    public function testAttachRole()
    {
        $role = $this->getObjectRandom(\App\Role::class);
        $params = [
            'role' => $role->name
        ];
        $client = $this->getObjectRandom(\App\Client::class);
        $this->post('clients/' . $client->id_clients . '/attachRole', $params)
            ->assertResponseStatus(200);
    }

    public function testSendPassword()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        $params = [
            'email' => $client->email,
            'name' => $client->name,
            'password' => 'password_test'
        ];
        $this->post('clients/sendPassword', $params)
            ->assertResponseStatus(200);
    }

    public function testSetLevel()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        $this->post('clients/' . $client->id_clients . '/attachRole')
            ->assertResponseStatus(200);
    }

    public function testCards()
    {
        // conekta not found client with the conekta token
        $client = $this->getObjectRandom(\App\Client::class);
        $this->get('clients/' . $client->id_clients . '/cards')
            ->assertResponseStatus(200);

    }

    public function testCardsCreate()
    {
        // conekta not found client with the conekta token
        $client = $this->getObjectRandom(\App\Client::class);
        $params = [
            'token' => $client->conekta_token
        ];
        $this->post('clients/' . $client->id_clients . '/cards', $params)
            ->assertResponseStatus(200);

    }

    public function testCardsShow()
    {
        // conekta not found client with the conekta token
        $client = $this->getObjectRandom(\App\Client::class);
        $id_card = 'testing_id';
        $this->get('clients/' . $client->id_clients . '/cards/' . $id_card)
            ->assertResponseStatus(200);

    }

    public function testCardsDelete()
    {
        // conekta not found client with the conekta token
        $client = $this->getObjectRandom(\App\Client::class);
        $id_card = 'testing_id';
        $this->delete('clients/' . $client->id_clients . '/cards/' . $id_card)
            ->assertResponseStatus(200);

    }

    public function testCardsSetAsDefault()
    {
        // conekta not found client with the conekta token
        $client = $this->getObjectRandom(\App\Client::class);
        $id_card = 'testing_id';
        $this->post('clients/' . $client->id_clients . '/cards/' . $id_card . '/setAsDefault')
            ->assertResponseStatus(200);

    }

    public function testMissingPolls()
    {
        $client = $this->getObjectRandom(\App\Client::class);
        $this->get('clients/' . $client->id_clients . '/missingPolls')
            ->assertResponseStatus(200);

    }

    public function testSwitchInvoice()
    {
        // conekta not found client with the conekta token
        $client = $this->getObjectRandom(\App\Client::class);
        $this->post('clients/' . $client->id_clients . '/switchInvoice')
            ->assertResponseStatus(200);

    }

    public function testRegisterCommissions()
    {
        $this->post('clients/registerCommissions')
            ->assertResponseStatus(200);

    }

}
